<?php

use App\Models\Supplier;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group([

    'middleware' => 'api',
    'namespace' => 'App\Http\Controllers',
    'prefix' => 'auth'

], function ($router) {

    Route::post('signup', 'AuthController@signup');
    Route::post('login', 'AuthController@login');
    Route::post('logout', 'AuthController@logout');
    Route::post('refresh', 'AuthController@refresh');
    Route::post('me', 'AuthController@me');
    // Authentication End

    // customer Authentication 
    Route::post('customer/registration', 'AuthController@customerRegistration');
    Route::post('customer/login', 'AuthController@customerLogin');
    // Route::post('/customer/registration', [App\Http\Controllers\Api\AuthController::class, 'customerRegistration'])->name('customer.registration');

});

// Employee
Route::resource('employee', App\Http\Controllers\Api\EmployeeController::class);
// Supplier
Route::resource('supplier', App\Http\Controllers\Api\SupplierController::class);
// Product
Route::resource('product', App\Http\Controllers\Api\ProductController::class);
Route::get('product/wise/category/variation', [App\Http\Controllers\Api\ProductController::class, 'productWiseCategoryList']);
Route::get('category/wise/variation', [App\Http\Controllers\Api\ProductController::class, 'catWiseVariationList']);
Route::get('variation/wise/variationoption', [App\Http\Controllers\Api\ProductController::class, 'variationWiseVariationList']);

// categoryWiseVariation 
Route::post('store/category/wise-variation', [\App\Http\Controllers\Api\ProductController::class, 'storeCategoryWiseVariation']);
Route::get('category/wise-variation/list', [\App\Http\Controllers\Api\ProductController::class, 'category_wise_variation']);
Route::get('/category/wise-variation/edit/{id?}', [\App\Http\Controllers\Api\ProductController::class, 'editCategoryWiseVariation']);

// Variation 
Route::post('store/variation', [\App\Http\Controllers\Api\ProductController::class, 'storeVariation']);
Route::get('category/variation/list', [\App\Http\Controllers\Api\ProductController::class, 'category_variation_List']);

// Expense
Route::resource('expense', App\Http\Controllers\Api\ExpenseController::class);
// Category
Route::get('category', [App\Http\Controllers\CategoryController::class, 'categories']);
Route::post('store/category', [App\Http\Controllers\CategoryController::class, 'storeCategory']);
Route::get('/category/delete/{id?}', [App\Http\Controllers\CategoryController::class, 'deleteCategory']);
Route::get('/category/edit/{id?}', [App\Http\Controllers\CategoryController::class, 'editCategory']);
Route::post('/category/update/{id?}', [App\Http\Controllers\CategoryController::class, 'updateCategory']);
// salary
Route::post('/payroll/{id?}', [App\Http\Controllers\Api\SalaryController::class, 'paySalary']);


//frontend 
Route::get('/product-details/{id?}', [App\Http\Controllers\FrontEndController::class, 'productDetails']);
Route::post('store/cartadd', [App\Http\Controllers\FrontEndController::class, 'storeCartItem']);

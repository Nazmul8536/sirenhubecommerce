<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\FrontEndController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('index');
// });

// frontend start 
route::get('/',[App\Http\Controllers\FrontEndController::class,'index'])->name('index');

// frontend end 

Route::get('/admin/login', function () {
    return view('welcome');
});



Route::get('/admin/{vue_capture?}', function () {
    return view('welcome');
})->where('vue_capture', '[\/\w\.-]*');


//whole sale order
// route::get("/wholesale-order",[App\Http\Controllers\FrontEndController::class,'wholeSale'])->name('wholesale-order');



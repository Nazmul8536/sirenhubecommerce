import * as Vue from 'vue';
import * as VueRouter from 'vue-router';
window.axios = require('axios');

import {routes} from './routes';
import {front} from './front';

// User js
import User from './Helpers/user';
import CustomHelper from './Helpers/customHelper';

window.User = User;
window.CustomHelper = CustomHelper;


// sweet alert 2 start
// ES6 Modules or TypeScript
import Swal from 'sweetalert2'
window.Swal = Swal;

const Toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3000,
    timerProgressBar: true,
    didOpen: (toast) => {
      toast.addEventListener('mouseenter', Swal.stopTimer)
      toast.addEventListener('mouseleave', Swal.resumeTimer)
    }
  })

  window.Toast = Toast;
// sweet alert 2 end

// Noty Start
import Notification from './Helpers/Notification';
window.Notification = Notification
// Noty end

const router = VueRouter.createRouter({
  history: VueRouter.createWebHistory(),
  routes,
  front
});

Vue.createApp(App).use(router).mount('#App');

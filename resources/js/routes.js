let login = require("./components/auth/login.vue").default;
let reg = require("./components/auth/register.vue").default;
let forget = require("./components/auth/forget.vue").default;
let home = require("./components/home.vue").default;
let logout = require("./components/auth/logout.vue").default;


// End Of Authentication

// Employee
let empList = require("./components/employee/employeeList.vue").default;
let employeeAdd = require("./components/employee/addNewEmployee.vue").default;
let employeeEdit = require("./components/employee/editEmployee.vue").default;
// Supplier
let addSupplier = require("./components/supplier/addSupplier.vue").default;
let supplierList = require("./components/supplier/supplierList.vue").default;
let editSupplier = require("./components/supplier/editSupplier.vue").default;
// Category
let addCategory = require("./components/category/addCategory.vue").default;
let categoryList = require("./components/category/categoryList.vue").default;
let editCategory = require("./components/category/editCategory.vue").default;
// Product
let addProduct = require("./components/product/addProduct.vue").default;
let productList = require("./components/product/productList.vue").default;
let editProduct = require("./components/product/editProduct.vue").default;
let variationProduct = require("./components/product/variationProduct.vue").default;

let categoryVariation = require("./components/product/categoryVariation.vue").default;
let categoryVariationList = require("./components/product/categoryVariationList.vue").default;
let editCategoryWiseVariation = require("./components/product/editCategoryVariation.vue").default;

let addVariation = require("./components/product/addvariation.vue").default;
let variationList = require("./components/product/variationList.vue").default;

// Product
let addExpense = require("./components/expense/addExpense.vue").default;
let expenseList = require("./components/expense/expenseList.vue").default;
let editExpense = require("./components/expense/editExpense.vue").default;
// Salary
let salary = require("./components/salary/empl_salary.vue").default;
let payNow = require("./components/salary/pay_now.vue").default;



// /frontend 
let homepage = require("./components/frontend/homepage.vue").default;
let wholesale = require("./components/frontend/wholesale.vue").default;
let product_details = require("./components/frontend/productDetails.vue").default;

let ucart = require("./components/frontend/cart/shoppingCart.vue").default;

// Authentication 
let ulogin = require("./components/frontend/authentication/signin.vue").default;
let ureg = require("./components/frontend/authentication/signup.vue").default;
let uforget = require("./components/frontend/authentication/forget.vue").default;
let customerLogout = require("./components/frontend/authentication/customerLogout.vue").default;
// let uhome = require("./components/frontend/authentication/homepage.vue").default;
// let ulogout = require("./components/frontend/authentication/homepage.vue").default;

export const routes = [
    { path: "/admin/login", component: login, name: "/login" },
    { path: "/admin/register", component: reg, name: "register" },
    { path: "/admin/forget", component: forget, name: "forget_password" },
    { path: "/admin/home", component: home, name: "home" },
    { path: "/admin/logout", component: logout, name: "logout" },
    // End Of Authentication

    // employee
    { path: "/add-employee", component: employeeAdd, name: "addEmployee" },
    { path: "/employee-list", component: empList, name: "employeeList" },
    { path: "/edit-employee/:id", component: employeeEdit, name: "edit-employee" },

    // supplier
    { path: "/admin/add-supplier", component: addSupplier, name: "SupplierAdd"},
    { path: "/admin/supplier-list", component: supplierList, name: "supplierList"},
    { path: "/admin/edit-supplier/:id", component: editSupplier, name: "edit-supplier"},

    // supplier
    { path: "/admin/add-category", component: addCategory, name: "categoryAdd"},
    { path: "/admin/category-list", component: categoryList, name: "categoryList"},
    { path: "/admin/category-edit/:id", component: editCategory, name: "editCategory"},

    // supplier
    { path: "/admin/add-product", component: addProduct, name: "productAdd"},
    { path: "/admin/product-list", component: productList, name: "productList"},
    { path: "/admin/product-variation", component: variationProduct, name: "variationProduct"},
    { path: "/admin/product-edit/:id", component: editProduct, name: "editProduct"},
    
    { path: "/admin/category/wise/variation", component: categoryVariation, name: "categoryVariation"},
    { path: "/admin/category/wise/variation/list", component: categoryVariationList, name: "categoryVariationList"},
    { path: "/admin/edit-category-wise-variation/:id", component: editCategoryWiseVariation, name: "editCategoryWiseVariation"},

    { path: "/admin/variation", component: addVariation, name: "addVariation"},
    { path: "/admin/variation-list", component: variationList, name: "variationList"},

    // Expense
    { path: "/add-expense", component: addExpense, name: "expenseAdd"},
    { path: "/expense-list", component: expenseList, name: "expenseList"},
    { path: "/expense-edit/:id", component: editExpense, name: "editExpense"},

    // Salary
    { path: "/salary", component: salary, name: "salary"},
    { path: "/salary/paynow/:id", component: payNow, name: "paynow"},

    //Dashboard Data 
    // { path: "/admin/supplier-list", component: supplierList, name: "supplierList"},
    
    
    // frontend 
    { path: "/", component: homepage, name: "/" },
    { path: "/wholesale", component: wholesale, name: "wholesale" },
    { path: "/product-details/:id", component: product_details, name: "productDetails" },
    { path: "/product/cart-user/:id?", component: ucart, name: "ucart" },
   
    //authentication
    { path: "/user/login", component: ulogin, name: "ulogin" },
    { path: "/user/register/:id?", component: ureg, name: "ureg" },
    { path: "/user/forget", component: uforget, name: "uforget" },    
    { path: "/user/customer/logout", component: customerLogout, name: "customer-logout" },

];

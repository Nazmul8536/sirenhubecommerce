
class Notification {
    success() {
        new Noty({
            type: "success",
            layout: "topRight",
            timeout: 1000,
            text: "Some notification text",
        }).show();
    }

    alert() {
        new Noty({
            type: "alert",
            layout: "topRight",
            timeout: 1000,
            text: "Some notification text",
        }).show();
    }

    error() {
        new Noty({
            type: "error",
            layout: "topRight",
            timeout: 1000,
            text: "Some notification text",
        }).show();
    }

    warning() {
        new Noty({
            type: "warning",
            layout: "topRight",
            timeout: 1000,
            text: "Some notification text",
        }).show();
    }

    img_validation() {
        new Noty({
            type: "success",
            layout: "topRight",
            text: "Some notification text",
            time: 200,
        }).show();
    }
}

export default Notification = new Notification();

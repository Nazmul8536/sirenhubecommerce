<div id="sidebar-wrapper" class="sidebar-wrapper">
    <nav class="sidebar-main">
        <div id="sidebar-menu">
            <ul class="sidebar-links" id="simple-bar">
                @if (!empty($categories))
                    @foreach ($categories as $key => $category)
                        <li class="sidebar-list">
                            <a class="sidebar-link sidebar-title link-nav" href="#">
                                <i
                                    class="fa-solid {{ !empty($category->category_img) ? $category->category_img : '' }}"></i>
                                <span>{{ !empty($category->category_name) ? $category->category_name : '' }}</span>
                            </a>
                        </li>
                    @endforeach
                @endif
            </ul>
        </div>
    </nav>
</div>
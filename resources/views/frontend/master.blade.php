@include('frontend.head')

<body>
    <div id="App">
        <!-- page-wrapper Start-->
        <div class="page-wrapper compact-wrapper" id="pageWrapper">
            <!-- Page Header Start-->
            @include('frontend.header')
            <!-- Page Header Ends-->

            <!-- Page Body Start-->
            <div class="page-body-wrapper">
                <!-- Page Sidebar Start-->
                @include('frontend.sidebar')
                <!-- Page Sidebar Ends-->
                <div class="page-body">
                    @yield('content')
                    <!-- gallery Start -->
                    <router-view></router-view>
                    <!-- gallery End -->
                </div>
            </div>
            @include('frontend.shoppingCart')
            @stack('shoppingcart')
            @include('frontend.footer')

        </div>
    </div>
</body>

@include('frontend.foot')

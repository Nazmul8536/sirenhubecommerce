<footer class="footer">
    <div class="top_footer">
        <div class="row">
            <div class="col-sm-6 col-md-6 col-lg-3">
                {{-- <img class="w-50" src="./assets/images/logo/logo_dark.png" alt="" /> --}}
                <h1
                    style="font-size: 22px;
            font-weight: 700;
            letter-spacing: 0;
            color: coral">
                    Siren Garments Hub</h1>

                <p>
                    If You Searching a local wear manufecturer who can satisfied you by making your desired
                    wear, We are Here
                    we have good facilities and experience about making traditional and western fancy
                    dresses.
                    You can contact us
                    for making customized dresses for your busness pages showroom buying house or export .
                    we
                    ensure 100% export
                    quality .
                </p>
            </div>

            <div class="col-sm-6 col-md-6 col-lg-3">
                <h3 class="footer_header">Information</h3>
                <ul>
                    <li><a href="#">About Us</a></li>
                    <li><a href="#">Privacy Policy</a></li>
                    <li><a href="#">Terms and Conditions</a></li>
                    <li><a href="#">Return and Refund Policy</a></li>
                </ul>
                <h3 class="footer_header mt-3">Social Links</h3>
                <ul class="social-link">
                    <li class="twitter">
                        <a href="https://twitter.com/mcareonline" data-toggle="tooltip" target="_blank"
                            title="" data-original-title="Twitter">
                            <i class="fa-brands fa-twitter"></i>
                        </a>
                    </li>
                    <li class="twitter">
                        <a href="https://www.linkedin.com/company/mcareonline" data-toggle="tooltip"
                            target="_blank" title="" data-original-title="Linkedin">
                            <i class="fa-brands fa-linkedin-in"></i>
                        </a>
                    </li>

                    <li class="facebook">
                        <a href="https://www.facebook.com/McareOnline" data-toggle="tooltip" target="_blank"
                            title="" data-original-title="Facebook">
                            <i class="fa-brands fa-facebook-f"></i>
                        </a>
                    </li>
                    <li class="youtube">
                        <a href="https://www.youtube.com/channel/UCy04iMG9zqelk0sAUfpRknw"
                            data-toggle="tooltip" target="_blank" title=""
                            data-original-title="Youtube">
                            <i class="fa-brands fa-youtube"></i>
                        </a>
                    </li>
                    <li class="instagram">
                        <a href="https://www.instagram.com/mcare_online/" data-toggle="tooltip"
                            target="_blank" title="" data-original-title="Instagram">
                            <i class="fa-brands fa-instagram"></i>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="col-sm-6 col-md-6 col-lg-3">
                <h3 class="footer_header">USEFUL LINKS</h3>
                <ul>
                    <li><a href="#">Facebook</a></li>
                    <li><a href="#">Instragram</a></li>
                    <li><a href="#">Twitter</a></li>
                    <li><a href="#">Linkdin</a></li>
                </ul>
            </div>
            <div class="col-sm-6 col-md-6 col-lg-3">
                <h3 class="footer_header">CONTACT US</h3>
                <ul>
                    <li>
                        <a href="#">
                            <strong>ADDRESS:</strong> Sisir Chala(Pantax Mor) <br> Gazipur Sadar, Gazipur
                            1740
                            .</a>
                    </li>
                    <li>
                        <a href="#"> <strong>PHONE: </strong> +880 1571191747</a>
                    </li>
                    <li>
                        <a href="#"> <strong>EMAIL: </strong> Siren.gh247@gmail.com</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="bottom_footer">
        <div class="row">
            <div class="col-md-12 footer-copyright text-center">
                <p class="mb-0">Copyright 2023 © Nazmul</p>
            </div>
        </div>
    </div>
</footer>

<!-- jQuery -->

<script src="{{ asset('js/app.js') }}"></script>
<!-- Bootstrap js-->
<script src="assets/js/jquery-1.12.4.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/js/slick.min.js"></script>
<script src="assets/js/scripts.js"></script>

<script>
    function toggleSidebar() {
        var element = document.getElementById("sidebar-wrapper");
        element.classList.toggle("close_icon");
    }

    if (window.innerWidth < 1500) {
        const initialAddedClass = document.getElementById("sidebar-wrapper");
        initialAddedClass.classList.add("close_icon");
    }

    $(document).ready(function($) {
        // Declare the body variable
        var $body = $("body");

        // Function that shows and hides the sidebar cart
        $(".cart-button, .close-button").click(function(e) {
            e.preventDefault();

            // Add the show-sidebar-cart class to the body tag
            $body.toggleClass("show-sidebar-cart");
        });

        // Function that adds or subtracts quantity when a
        // plus or minus button is clicked
        $body.on("click", ".plus-button, .minus-button", function() {
            // Get quanitity input values
            var qty = $(this).closest(".qty").find(".qty-input");
            var val = parseFloat(qty.val());
            var max = parseFloat(qty.attr("max"));
            var min = parseFloat(qty.attr("min"));
            var step = parseFloat(qty.attr("step"));

            // Check which button is clicked
            if ($(this).is(".plus-button")) {
                // Increase the value
                qty.val(val + step);
            } else {
                // Check if minimum button is clicked and that value is
                // >= to the minimum required
                if (min && min >= val) {
                    // Do nothing because value is the minimum required
                    qty.val(min);
                } else if (val > 0) {
                    // Subtract the value
                    qty.val(val - step);
                }
            }
        });
    });




    // header Part 
    let userValue = '';
    if (!User.logedIn()) {
        userValue = '';
    } else {
        userValue = User.name();
    }
    $('.userValue').html(userValue)

    let token = localStorage.getItem('token');
    if (token) {
        // $("#left_col").css('display', '');
        // $("#top_nav").css('display', '');
        // $("#App").css('background', '');
        // $(".right_col").css('margin-left', '');
        $("#hSignUp").css('display', 'none');
    }else{
        $("#hLoginDt").css('display', 'none');
        
    }
</script>

</html>

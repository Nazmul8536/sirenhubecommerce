@php
    $project_data = DB::table('project_details')->first();
@endphp
<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="icon" href="assets/images/logo/favicon-icon.png" type="image/x-icon" />
    <link rel="shortcut icon" href="assets/images/logo/favicon-icon.png" type="image/x-icon" />
    <title>{{ !empty($data->project_name) ? ucfirst($data->project_name) : '' }}</title>
    <!-- Google font-->
    <link rel="preconnect" href="https://fonts.googleapis.com/" />
    <link rel="preconnect" href="https://fonts.gstatic.com/" crossorigin="" />
    <link
        href="https://fonts.googleapis.com/css2?family=Rubik:ital,wght@0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,300;1,400;1,500;1,600;1,700;1,800;1,900&amp;display=swap"
        rel="stylesheet" />
    <link
        href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&amp;display=swap"
        rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="assets/css/vendors/all.min.css" />
    <!-- Bootstrap css-->
    <link rel="stylesheet" type="text/css" href="assets/css/vendors/bootstrap.min.css" />
    <!-- Style css-->
    <link rel="stylesheet" type="text/css" href="assets/css/style.css" />
    <!-- Responsive css-->
    <link rel="stylesheet" type="text/css" href="assets/css/responsive.css" />
    <link rel="stylesheet" type="text/css" href="assets/css/slick.css" />
    <link rel="stylesheet" type="text/css" href="assets/css/slick-theme.css" />
    <!-- Custom css-->
    <link rel="stylesheet" type="text/css" href="assets/css/custom.css" />
    <link rel="stylesheet" type="text/css" href="assets/css/custom_responsive.css" />
    <style>
        label.custom-file-label.form-control.upload_app_label {
            line-height: 1.7;
        }
    </style>

</head>

<body>
    <!-- page-wrapper Start-->
    <div class="page-wrapper compact-wrapper" id="pageWrapper">
        <!-- Page Header Start-->
        <div class="page-header">
            <div class="header-wrapper row align-items-center m-0 justify-content-between">
                <div class="col-sm-12 col-md-3 col-lg-3 col-xl-3">
                    <div class="header-logo-wrapper p-0">
                        <div class="logo" style="width: 85px">
                            <img class="img-fluid" src="{{ asset('frontend/others/logo.jpg') }}" alt=""
                                style="border-radius: 50%" />
                        </div>
                    </div>
                </div>

                <div class="col-sm-12 col-md-5 col-lg-5 col-xl-6">
                    <div class="input-group">
                        <span class="input-group-text" id="basic-addon1">
                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                xmlns="http://www.w3.org/2000/svg">
                                <g>
                                    <g>
                                        <path fill-rule="evenodd" clip-rule="evenodd"
                                            d="M11.2753 2.71436C16.0029 2.71436 19.8363 6.54674 19.8363 11.2753C19.8363 16.0039 16.0029 19.8363 11.2753 19.8363C6.54674 19.8363 2.71436 16.0039 2.71436 11.2753C2.71436 6.54674 6.54674 2.71436 11.2753 2.71436Z"
                                            stroke-width="2" stroke-linecap="round" stroke-linejoin="round"></path>
                                        <path fill-rule="evenodd" clip-rule="evenodd"
                                            d="M19.8987 18.4878C20.6778 18.4878 21.3092 19.1202 21.3092 19.8983C21.3092 20.6783 20.6778 21.3097 19.8987 21.3097C19.1197 21.3097 18.4873 20.6783 18.4873 19.8983C18.4873 19.1202 19.1197 18.4878 19.8987 18.4878Z"
                                            stroke-width="2" stroke-linecap="round" stroke-linejoin="round"></path>
                                    </g>
                                </g>
                            </svg>
                        </span>
                        <input class="form-control" type="text" placeholder="Search here.." aria-label="search"
                            aria-describedby="basic-addon1" />
                    </div>
                </div>

                <div class="col-sm-12 col-md-4 col-lg-4 col-xl-3">
                    <div class="nav-right right-header p-0">
                        <ul class="nav-menus">
                            <li class="cart-nav">
                                <div class="toggle-sidebar" onclick="toggleSidebar()">
                                    <div class="status_toggle sidebar-toggle d-flex">
                                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                            xmlns="http://www.w3.org/2000/svg">
                                            <g>
                                                <g>
                                                    <path fill-rule="evenodd" clip-rule="evenodd"
                                                        d="M21.0003 6.6738C21.0003 8.7024 19.3551 10.3476 17.3265 10.3476C15.2979 10.3476 13.6536 8.7024 13.6536 6.6738C13.6536 4.6452 15.2979 3 17.3265 3C19.3551 3 21.0003 4.6452 21.0003 6.6738Z"
                                                        stroke="#130F26" stroke-width="1.5" stroke-linecap="round"
                                                        stroke-linejoin="round"></path>
                                                    <path fill-rule="evenodd" clip-rule="evenodd"
                                                        d="M10.3467 6.6738C10.3467 8.7024 8.7024 10.3476 6.6729 10.3476C4.6452 10.3476 3 8.7024 3 6.6738C3 4.6452 4.6452 3 6.6729 3C8.7024 3 10.3467 4.6452 10.3467 6.6738Z"
                                                        stroke="#130F26" stroke-width="1.5" stroke-linecap="round"
                                                        stroke-linejoin="round"></path>
                                                    <path fill-rule="evenodd" clip-rule="evenodd"
                                                        d="M21.0003 17.2619C21.0003 19.2905 19.3551 20.9348 17.3265 20.9348C15.2979 20.9348 13.6536 19.2905 13.6536 17.2619C13.6536 15.2333 15.2979 13.5881 17.3265 13.5881C19.3551 13.5881 21.0003 15.2333 21.0003 17.2619Z"
                                                        stroke="#130F26" stroke-width="1.5" stroke-linecap="round"
                                                        stroke-linejoin="round"></path>
                                                    <path fill-rule="evenodd" clip-rule="evenodd"
                                                        d="M10.3467 17.2619C10.3467 19.2905 8.7024 20.9348 6.6729 20.9348C4.6452 20.9348 3 19.2905 3 17.2619C3 15.2333 4.6452 13.5881 6.6729 13.5881C8.7024 13.5881 10.3467 15.2333 10.3467 17.2619Z"
                                                        stroke="#130F26" stroke-width="1.5" stroke-linecap="round"
                                                        stroke-linejoin="round"></path>
                                                </g>
                                            </g>
                                        </svg>
                                    </div>
                                </div>
                            </li>
                            {{-- <li class="cart-nav">
                                <div class="deliverAddress">
                                    <div>
                                        <p class="mb-0">Hello, </p>
                                        <p class="mb-0">Mcare Cash: ৳0</p>
                                    </div>
                                </div>
                            </li>
                            <li class="cart-nav" data-bs-toggle="modal" data-bs-target="#loginModal">
                                <div class="deliverAddress">
                                    <div>
                                        <p class="mb-0">Refer</p>
                                        <p class="mb-0">& Earn</p>
                                    </div>
                                </div>
                            </li> --}}

                            <li class="cart-nav onhover-dropdown cart-button">
                                <div class="cart-box">
                                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                        xmlns="http://www.w3.org/2000/svg">
                                        <g>
                                            <g>
                                                <path
                                                    d="M5.52377 7C9.41427 5.74386 13.9724 5.45573 16 5.5C18.0276 5.54427 18.8831 6.2663 19.5 7.5C20.5 9.5 20.289 14.4881 18.5 16.0871C16.712 17.6861 9.33015 17.8381 6.87015 16.0871C4.27115 14.2361 5.629 9.192 5.544 5.743C5.595 3.813 3.5 3.5 3.5 3.5"
                                                    stroke="#130F26" stroke-width="1.5" stroke-linecap="round"
                                                    stroke-linejoin="round"></path>
                                                <path d="M13 10.5H15.773" stroke="#130F26" stroke-width="1.5"
                                                    stroke-linecap="round" stroke-linejoin="round"></path>
                                                <path fill-rule="evenodd" clip-rule="evenodd"
                                                    d="M7.26399 20.1274C7.56399 20.1274 7.80799 20.3714 7.80799 20.6714C7.80799 20.9724 7.56399 21.2164 7.26399 21.2164C6.96299 21.2164 6.71899 20.9724 6.71899 20.6714C6.71899 20.3714 6.96299 20.1274 7.26399 20.1274Z"
                                                    fill="#130F26" stroke="#130F26" stroke-width="1.5"
                                                    stroke-linecap="round" stroke-linejoin="round"></path>
                                                <path fill-rule="evenodd" clip-rule="evenodd"
                                                    d="M17.5954 20.1274C17.8964 20.1274 18.1404 20.3714 18.1404 20.6714C18.1404 20.9724 17.8964 21.2164 17.5954 21.2164C17.2954 21.2164 17.0514 20.9724 17.0514 20.6714C17.0514 20.3714 17.2954 20.1274 17.5954 20.1274Z"
                                                    fill="#130F26" stroke="#130F26" stroke-width="1.5"
                                                    stroke-linecap="round" stroke-linejoin="round"></path>
                                            </g>
                                        </g>
                                    </svg>
                                    <span class="badge rounded-pill badge-primary">2</span>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="bottom-header">
                <div class="toggle-sidebar" onclick="toggleSidebar()">
                    <div class="status_toggle sidebar-toggle d-flex">
                        <i class="fa-solid fa-bars"></i>
                        <span>Categories</span>
                    </div>
                </div>
                <div class="call_to_order">
                    <a class="" href="Tel:01894947373">
                        <i class="fa-solid fa-phone"></i>
                        Call To Order
                    </a>
                    <a class="" href="#">
                        <i class="fa-solid fa-heart"></i>
                        Healthcare Product
                    </a>
                </div>
                <div class="quick_order" data-bs-toggle="modal" data-bs-target="#uploadPrescriptionModal">

                    <a class="btn btn-md btn-outline-primary " href="{{ route('wholesale-order') }}">
                        <span class="" style="color: white">WholeSale Order</span>
                    </a>
                    <span> To quick order</span>
                    <span class="">
                        <i class="fa-solid fa-file-prescription"></i>
                        Upload Prescription
                    </span>
                </div>
            </div>
        </div>
        <!-- Page Header Ends-->

        <!-- Page Body Start-->
        <div class="page-body-wrapper">
            <!-- Page Sidebar Start-->
            <div id="sidebar-wrapper" class="sidebar-wrapper">
                <nav class="sidebar-main">
                    <div id="sidebar-menu">
                        <ul class="sidebar-links" id="simple-bar">
                            @if (!empty($categories))
                                @foreach ($categories as $key => $category)
                                    <li class="sidebar-list">
                                        <a class="sidebar-link sidebar-title link-nav" href="#">
                                            <i
                                                class="fa-solid {{ !empty($category->category_img) ? $category->category_img : '' }}"></i>
                                            <span>{{ !empty($category->category_name) ? $category->category_name : '' }}</span>
                                        </a>
                                    </li>
                                @endforeach
                            @endif
                        </ul>
                    </div>
                </nav>
            </div>
            <!-- Page Sidebar Ends-->











            <!-- Page Sidebar Ends-->
            <div class="page-body">
                <!-- Container-fluid starts-->
                <div class="container-fluid default-dash">
                    <div class="row justify-content-center">
                        <div class="col-md-12">
                            <div class="card mt-3">
                                <div class="card-body">
                                    <div class="row justify-content-center align-items-center">

                                        <form action="" method="post" id="formID">
                                            @csrf

                                            <div class="card-body p-2">
                                                <div class="row">
                                                    <div class="col-12 table-responsive">
                                                        <div class="card">
                                                            <div class="card-header bg-primary">

                                                                {{-- <div class="">
                                                                    <h5 class="mt-2 mb-0 float-left"> Add Product</h5>
                                                                    <span id="med_td_action_product_0"
                                                                        onclick="addNewProduct(this)"
                                                                        class="btn btn-md btn-outline-success float-right mt-2">
                                                                        +Add
                                                                    </span> --}}
                                                                <div class="col-md-12 row">
                                                                    <div class="col-md-8">
                                                                        <div class="float-left">
                                                                            <h5 class="mt-2 mb-0 float-left"> Add
                                                                                Product
                                                                            </h5>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-4">
                                                                        <div id="top-action">
                                                                            <span id="med_td_action_product_0"
                                                                                style="float: right;"
                                                                                onclick="addNewProduct(this)"
                                                                                class="btn btn-md btn-outline-success float-right mt-2">
                                                                                +Add New Product
                                                                            </span>
                                                                        </div>
                                                                    </div>


                                                                </div>

                                                            </div>
                                                        </div>
                                                        <div class="card-body 0" style="border:1px solid aqua"
                                                            id="table_dt">
                                                            <table
                                                                class="table table-striped table-bordered table-hover">
                                                                <thead class="global-uhl-table-bg">
                                                                    <tr>
                                                                        {{-- <th>Photo</th> --}}
                                                                        {{-- <th>Color</th> --}}
                                                                        <th>Size</th>
                                                                        <th>Quantity</th>
                                                                        <th>Comment</th>
                                                                        <th></th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody id="data-append-to">
                                                                    <div class="custom-file">
                                                                        <div class="col-md-12 row">
                                                                            <div class="col-md-8">
                                                                                <div class="float-left">
                                                                                    <input type="file"
                                                                                        class="custom-file-input form-control"
                                                                                        id="upload_app"
                                                                                        name="upload_" />
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-4">
                                                                                <div id="top-action">
                                                                                    <input type="text"
                                                                                        class="form-control"
                                                                                        id="color_0" name="color"
                                                                                        placeholder="Enter color">
                                                                                </div>
                                                                            </div>


                                                                        </div>




                                                                    </div>
                                                                    <tr id="0">

                                                                        {{-- <td>
                                                                            
                                                                        </td> --}}

                                                                        <td>
                                                                            <input type="text" class="form-control"
                                                                                id="size_0" name="size[]"
                                                                                placeholder="Enter Size">
                                                                        </td>
                                                                        <td>
                                                                            {{-- <input type="text" class="form-control qty_dt"
                                                                                id="quantity_0" name="quantity[]"
                                                                                onkeyup="setQty(this)"
                                                                                placeholder="Enter Quantity"
                                                                                value='1'> --}}
                                                                                <input type="text"
                                                            class="form-control mb-0 text-right hdn_item_qnty"
                                                            onkeyup="setQty(this)"
                                                            id="quantity_0"
                                                            name="hdn_item_qnty[]" value="1">
                                                                        </td>
                                                                        <td>
                                                                            <input type="text" class="form-control"
                                                                                id="comment_0" name="comment[]"
                                                                                placeholder="Enter Comment"
                                                                                maxlength="100">
                                                                        </td>

                                                                        <td>
                                                                            <span id="med_td_action_0"
                                                                                onclick="addNewRow(this)"
                                                                                class="btn btn-md btn-outline-success">
                                                                                +Add
                                                                            </span>
                                                                        </td>
                                                                    </tr>



                                                                </tbody>
                                                                <tr>
                                                                    <td style="text-align:right"class="text-right">
                                                                        Total Quantity</td>
                                                                    <td><span id="t_qty">0</span></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                </tr>

                                                                <br>
                                                            </table>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                    </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="page-body">
                <!-- category Start -->
                <div id="category">
                    <div class="container-fluid category-slider">
                        <div class="category-arrow">
                            <a class="right" rel="alternate" href="#" hreflang="en-BD">
                                <i class="fa fa-angle-right" aria-hidden="true"></i>
                            </a>
                            <a class="left" rel="alternate" href="#" hreflang="en-BD">
                                <i class="fa fa-angle-left" aria-hidden="true"></i>
                            </a>
                        </div>
                        <div class="row category-slider-row">
                            <div class="category-item">
                                <a rel="alternate" href="#" hreflang="en-BD">
                                    <img class="mx-auto d-block" src="assets/images/category/covid19.png"
                                        alt="Covid-19 Special" title="Covid-19 Special" />
                                    <p class="category-item-title">Covid-19 Special</p>
                                </a>
                            </div>
                            <div class="category-item">
                                <a rel="alternate" href="#" hreflang="en-BD">
                                    <img class="mx-auto d-block" src="assets/images/category/women.png"
                                        alt="Women Care" title="Women Care" />
                                    <p class="category-item-title">Women Care</p>
                                </a>
                            </div>
                            <div class="category-item">
                                <a rel="alternate" href="#" hreflang="en-BD">
                                    <img class="mx-auto d-block" src="assets/images/category/medical_devices.png"
                                        alt="Devices" title="Devices" />
                                    <p class="category-item-title">Devices</p>
                                </a>
                            </div>
                            <div class="category-item">
                                <a rel="alternate" href="#" hreflang="en-BD">
                                    <img class="mx-auto d-block" src="assets/images/category/sexual.png"
                                        alt="Sexual Wellness" title="Sexual Wellness" />
                                    <p class="category-item-title">Sexual Wellness</p>
                                </a>
                            </div>
                            <div class="category-item">
                                <a rel="alternate" href="#" hreflang="en-BD">
                                    <img class="mx-auto d-block" src="assets/images/category/harbal.png"
                                        alt="Herbal and Homeopathy" title="Herbal and Homeopathy" />
                                    <p class="category-item-title">Herbal and Homeopathy</p>
                                </a>
                            </div>
                            <div class="category-item">
                                <a rel="alternate" href="#" hreflang="en-BD">
                                    <img class="mx-auto d-block" src="assets/images/category/baby&mom.png"
                                        alt="Baby & Mom care" title="Baby & Mom care" />
                                    <p class="category-item-title">Baby & Mom care</p>
                                </a>
                            </div>
                            <div class="category-item">
                                <a rel="alternate" href="#" hreflang="en-BD">
                                    <img class="mx-auto d-block" src="assets/images/category/energy-drink.png"
                                        alt="Nutrition and drinks" title="Nutrition and drinks" />
                                    <p class="category-item-title">Nutrition and drinks</p>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <aside id="sidebar-cart">
            <main>
                <a href="#" class="close-button"><span class="close-icon">X</span></a>
                <h2>Shopping Bag <span class="count">2</span></h2>
                <ul class="products">
                    <li class="product">
                        <a href="#" class="cartProductLink">
                            <span class="product-image">
                                <img src="assets/images/checkout/01.webp" alt="Product Photo" />
                            </span>
                            <span class="product-details">
                                <h3>Very Cool Product One</h3>
                                <span class="qty-price">
                                    <span class="qty">
                                        <button class="minus-button" id="minus-button-1">-</button>
                                        <input type="number" id="qty-input-1" class="qty-input" step="1"
                                            min="1" max="1000" name="qty-input" value="1"
                                            pattern="[0-9]*" title="Quantity" inputmode="numeric" />
                                        <button class="plus-button" id="plus-button-1">+</button>
                                        <input type="hidden" name="item-price" id="item-price-1" value="12.00" />
                                    </span>
                                    <span class="price">৳ 16.00</span>
                                </span>
                            </span>
                        </a>
                        <a href="#remove" class="remove-button"><span class="remove-icon">X</span></a>
                    </li>

                    <li class="product">
                        <a href="#" class="cartProductLink">
                            <span class="product-image">
                                <img src="assets/images/checkout/02.webp" alt="Product Photo" />
                            </span>
                            <span class="product-details">
                                <h3>Very Cool Product Two...</h3>
                                <span class="qty-price">
                                    <span class="qty">
                                        <button class="minus-button" id="minus-button-1">-</button>
                                        <input type="number" id="qty-input-2" class="qty-input" step="1"
                                            min="1" max="1000" name="qty-input" value="1"
                                            pattern="[0-9]*" title="Quantity" inputmode="numeric" />
                                        <button class="plus-button" id="plus-button-1">+</button>
                                        <input type="hidden" name="item-price" id="item-price-2" value="12.00" />
                                    </span>
                                    <span class="price">৳ 28.00</span>
                                </span>
                            </span>
                        </a>
                        <a href="#remove" class="remove-button"><span class="remove-icon">X</span></a>
                    </li>

                    <li class="product">
                        <a href="#" class="cartProductLink">
                            <span class="product-image">
                                <img src="assets/images/checkout/03.webp" alt="Product Photo" />
                            </span>
                            <span class="product-details">
                                <h3>Very Cool Product Three</h3>
                                <span class="qty-price">
                                    <span class="qty">
                                        <form action="#" name="qty-form" id="qty-form-1">
                                            <button class="minus-button" id="minus-button-1">
                                                -
                                            </button>
                                            <input type="number" id="qty-input-3" class="qty-input" step="1"
                                                min="1" max="1000" name="qty-input" value="1"
                                                pattern="[0-9]*" title="Quantity" inputmode="numeric" />
                                            <button class="plus-button" id="plus-button-1">+</button>
                                            <input type="hidden" name="item-price" id="item-price-3"
                                                value="12.00" />
                                        </form>
                                    </span>
                                    <span class="price">৳ 12.00</span>
                                </span>
                            </span>
                        </a>
                        <a href="#remove" class="remove-button"><span class="remove-icon">X</span></a>
                    </li>
                </ul>

                <div class="other-items-in-cart">
                    <div class="coupon">
                        <ul class="nav nav-pills" id="pills-tab" role="tablist">
                            <li class="" role="presentation">
                                <button class="btn btn-link p-0" id="pills-cartCoupon-tab" data-bs-toggle="pill"
                                    data-bs-target="#pills-cartCoupon" type="button" role="tab"
                                    aria-controls="pills-cartCoupon" aria-selected="true">
                                    Have a coupon code?
                                </button>
                            </li>
                        </ul>
                        <div class="tab-content p-0 m-0" id="pills-tabContent">
                            <div class="tab-pane fade" id="pills-cartCoupon" role="tabpanel"
                                aria-labelledby="pills-cartCoupon-tab">
                                <div class="input-group mb-3">
                                    <input type="text" class="form-control" placeholder="Enter coupon Code" />
                                    <button class="btn btn-primary" type="button" id="button-addon2">
                                        Apply
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="headline">
                        <div class="savingTakaHighlights">
                            <div class="savingTakaHighlight">
                                <img src="https://cdn6.arogga.com/img/taka.png" alt="saving" />
                                <span>
                                    You are saving <strong>৳ 772.3</strong>
                                    in this order.
                                </span>
                            </div>

                            <div class="savingTakaHighlight">
                                <img src="https://cdn6.arogga.com/img/taka.png" alt="receive cashback" />
                                <span>
                                    You will receive <strong>৳ 20</strong>
                                    cashback for this order after delivery.
                                </span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="totals">
                    <div class="subtotal">
                        <span class="amountLabel">Subtotal:</span>
                        <span class="amount">৳ 54.00</span>
                    </div>
                    <div class="shipping">
                        <span class="amountLabel">Shipping:</span>
                        <span class="amount">৳ 7.95</span>
                    </div>
                    <div class="tax">
                        <span class="amountLabel">Tax:</span>
                        <span class="amount">৳ 71.95</span>
                    </div>
                    <div class="amountPayable">
                        <span class="amountLabel">Amont Payable:</span>
                        <span class="amount">৳ 71.95</span>
                    </div>
                </div>
                <div class="action-buttons">
                    <a class="checkout-button btn-primary" href="#">Proceed to checkout</a>
                </div>
            </main>
        </aside>

        <!-- footer start-->
        <footer class="footer">
            <div class="top_footer">
                <div class="row">
                    <div class="col-sm-6 col-md-6 col-lg-3">
                        <img class="w-50" src="./assets/images/logo/logo_dark.png" alt="" />

                        <p>
                            MCARE is an online healthcare platform, established by
                            professionals with over 10 years experience in the field of
                            education, training, IT business with local & global companies,
                            providing an affordable, hassle-free experience for all your
                            health needs.
                        </p>
                    </div>

                    <div class="col-sm-6 col-md-6 col-lg-3">
                        <h3 class="footer_header">Information</h3>
                        <ul>
                            <li><a href="#">About Us</a></li>
                            <li><a href="#">Privacy Policy</a></li>
                            <li><a href="#">Terms and Conditions</a></li>
                            <li><a href="#">Return and Refund Policy</a></li>
                        </ul>
                        <h3 class="footer_header mt-3">Social Links</h3>
                        <ul class="social-link">
                            <li class="twitter">
                                <a href="https://twitter.com/mcareonline" data-toggle="tooltip" target="_blank"
                                    title="" data-original-title="Twitter">
                                    <i class="fa-brands fa-twitter"></i>
                                </a>
                            </li>
                            <li class="twitter">
                                <a href="https://www.linkedin.com/company/mcareonline" data-toggle="tooltip"
                                    target="_blank" title="" data-original-title="Linkedin">
                                    <i class="fa-brands fa-linkedin-in"></i>
                                </a>
                            </li>

                            <li class="facebook">
                                <a href="https://www.facebook.com/McareOnline" data-toggle="tooltip" target="_blank"
                                    title="" data-original-title="Facebook">
                                    <i class="fa-brands fa-facebook-f"></i>
                                </a>
                            </li>
                            <li class="youtube">
                                <a href="https://www.youtube.com/channel/UCy04iMG9zqelk0sAUfpRknw"
                                    data-toggle="tooltip" target="_blank" title=""
                                    data-original-title="Youtube">
                                    <i class="fa-brands fa-youtube"></i>
                                </a>
                            </li>
                            <li class="instagram">
                                <a href="https://www.instagram.com/mcare_online/" data-toggle="tooltip"
                                    target="_blank" title="" data-original-title="Instagram">
                                    <i class="fa-brands fa-instagram"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-sm-6 col-md-6 col-lg-3">
                        <h3 class="footer_header">USEFUL LINKS</h3>
                        <ul>
                            <li><a href="#">FAQ</a></li>
                            <li><a href="#">Account</a></li>
                            <li><a href="#">Medicines</a></li>
                            <li><a href="#">Special Offers</a></li>
                        </ul>
                    </div>
                    <div class="col-sm-6 col-md-6 col-lg-3">
                        <h3 class="footer_header">CONTACT US</h3>
                        <ul>
                            <li>
                                <a href="#">
                                    <strong>ADDRESS:</strong> Wasi Tower(13th Floor), 572/K, ECB
                                    Chattar, Matikata, Dhaka Cantonment, Dhaka-1206, Bangladesh</a>
                            </li>
                            <li>
                                <a href="#"> <strong>PHONE: </strong> +880 18949 47373</a>
                            </li>
                            <li>
                                <a href="#"> <strong>EMAIL: </strong> support@mcare.com.bd</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="bottom_footer">
                <div class="row">
                    <div class="col-md-12 footer-copyright text-center">
                        <p class="mb-0">Copyright 2022 © MCARE</p>
                    </div>
                </div>
            </div>
        </footer>
        <!-- Bootstrap js-->
        <script src="assets/js/jquery-1.12.4.min.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
        <script src="assets/js/slick.min.js"></script>
        <script src="assets/js/scripts.js"></script>
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.3.0/jquery.form.js"></script>

        <script>
            function toggleSidebar() {
                var element = document.getElementById("sidebar-wrapper");
                element.classList.toggle("close_icon");
            }

            if (window.innerWidth < 1500) {
                const initialAddedClass = document.getElementById("sidebar-wrapper");
                initialAddedClass.classList.add("close_icon");
            }

            $(document).ready(function($) {
                // Declare the body variable
                var $body = $("body");

                // Function that shows and hides the sidebar cart
                $(".cart-button, .close-button").click(function(e) {
                    e.preventDefault();

                    // Add the show-sidebar-cart class to the body tag
                    $body.toggleClass("show-sidebar-cart");
                });

                // Function that adds or subtracts quantity when a
                // plus or minus button is clicked
                $body.on("click", ".plus-button, .minus-button", function() {
                    // Get quanitity input values
                    var qty = $(this).closest(".qty").find(".qty-input");
                    var val = parseFloat(qty.val());
                    var max = parseFloat(qty.attr("max"));
                    var min = parseFloat(qty.attr("min"));
                    var step = parseFloat(qty.attr("step"));

                    // Check which button is clicked
                    if ($(this).is(".plus-button")) {
                        // Increase the value
                        qty.val(val + step);
                    } else {
                        // Check if minimum button is clicked and that value is
                        // >= to the minimum required
                        if (min && min >= val) {
                            // Do nothing because value is the minimum required
                            qty.val(min);
                        } else if (val > 0) {
                            // Subtract the value
                            qty.val(val - step);
                        }
                    }
                });
            });


            function addNewRow(thisElement) {

                var row = $(thisElement).parents("tr").clone();
                var oldId = Number($(thisElement).parents("tr").attr("id"));
                var newId = $(thisElement).parents("#data-append-to").find("tr").length + 1;
                row.attr('id', newId);
                $(thisElement).parents('tbody').find('#size_' + oldId).val('');
                $(thisElement).parents('tbody').find('#quantity_' + oldId).val('');
                $(thisElement).parents('tbody').find('#comment_' + oldId).val('');
                row.find('#med_td_action_' + oldId).attr('id', 'med_td_action_' + newId);
                $(thisElement).parents("#data-append-to").append(row);
                $('#med_td_action_' + newId).html(
                    "<span class='btn btn-outline-danger btn-sm' onclick='removeTableRowOnly(this)'> <i class='fa fa-times'></i> </span>"
                );


                $("input[class='quantity_']").each(function() {
                    if ($(this).val() != '') {
                        qty += $(this).val();

                        console.log(qty)

                        $('#total_payment_amt').val(amt);
                        $('#pay_amount').val(amt);
                    }
                });

            }

            function setQty(element) {
console.log($(this).find(".hdn_item_qnty").val())
                var qty = $(element).val() != "" ? parseFloat($(element).val()) : 0;
                var total_qty = 0;
                // var item_id = $(element).parents("td").parents('tr').find(".item_id").val();
                // var item_price = parseFloat($('#item_rate_' + item_id).val());
                // var ipd_charge_prc = parseFloat($(element).parents('td').parents('tr').find('.ipd_charge').attr(
                //     'data-charge_prc')) > 0 && item_price > 0 ? item_price * parseFloat($(element).parents('td').parents(
                //     'tr').find('.ipd_charge').attr('data-charge_prc')) / 100 : 0;
                // var ipd_charge = ipd_charge_prc;
                // $(element).parents('td').parents('tr').find('.ipd_charge').val(ipd_charge);
                // $(element).parents('td').parents('tr').find('.ipd_charge_amt').val(ipd_charge);

                // var ipd_charge =parseFloat($(element).parents("tr").find(".ipd_charge").val()) ;

                // var vat = parseFloat($('#vat_pers_' + item_id).val()) * qty;
                // var total = 0;
                // var urgent_fee = 0;
                // var selectedValue = $(element).parents("td").parents("tr").find('select[name="item_delivery_status[]"]').val();
                // if (selectedValue == 2) {
                //     var urgent_fee = parseFloat($(element).parents("tr").find(".hdn_item_urgent_fee").val()) * qty;
                // } else {
                //     var urgent_fee = 0;
                // }
                // $('#hdn_item_total_qnty_' + item_id).val((item_price * qty) + vat + (ipd_charge * qty));


                $("input[class='hdn_item_qnty']").each(function() {
                    total_qty += $(this).find(".hdn_item_qnty").val() * 1;
                    console.log(total_qty)
                });

                $('#t_qty').html(qty);

                // alert(qty)
            }


            function addNewProduct(thisElement) {

                var table = $('#table_dt').clone();
                // console.log(table)
                var oldId = Number($(thisElement).parents("tr").attr("id"));
                // var newId = $(thisElement).parents("#data-append-to").find("tr").length + 1;
                // table.attr('id', newId);
                // $(thisElement).parents('tbody').find('#color_' + oldId).val('');
                // $(thisElement).parents('tbody').find('#quantity_' + oldId).val('');
                // $(thisElement).parents('tbody').find('#comment_' + oldId).val('');
                // table.find('#med_td_action_' + oldId).attr('id', 'med_td_action_' + newId);
                $('#table_dt').append(table);
                // $('#med_td_action_' + newId).html(
                //     "<span class='btn btn-outline-danger btn-sm' onclick='removeTableRowOnly(this)'> <i class='fa fa-times'></i> </span>"
                // );

            }

            function removeTableRowOnly(thisElement) {
                if (confirm("Are you sure?")) {
                    $(thisElement).parents("tr").remove();
                }
                return;
            }
        </script>
</body>

</html>

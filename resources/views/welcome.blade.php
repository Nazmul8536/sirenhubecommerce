@php
    $data = DB::table('project_details')
        ->join('users', 'project_details.project_no_pk', 'users.project_no_fk')
        ->where('project_details.status', 1)
        ->first();
@endphp
<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    {{-- csrf token --}}
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>{{ !empty($data->project_name) ? $data->project_name : '' }}</title>

    {{-- app.css link --}}
    <link href="{{ asset('css/app.css') }}" type="text/css" rel="stylesheet" />
    <!-- Bootstrap -->
    <link href="{{ asset('backend/vendors/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="{{ asset('backend/vendors/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
    <!-- NProgress -->
    <link href="{{ asset('backend/vendors/nprogress/nprogress.css') }}" rel="stylesheet">
    <!-- bootstrap-wysiwyg -->
    <link href="{{ asset('backend/vendors/google-code-prettify/bin/prettify.min.css') }}" rel="stylesheet">
    <!-- Custom styling plus plugins -->
    <link href="{{ asset('backend/build/css/custom.min.css') }}" rel="stylesheet">


</head>

<body class="nav-md" style="background:#f7f7f7"
    v-show="$route.path === '/admin/login' || $route.path === '/admin/register' || $route.path === '/admin/forget'?true:true">

    <div id="App">
        <div class="container body">
            <div class="main_container">
                <div class="col-md-3 left_col" id="left_col" style="display:none;"
                    v-show="$route.path === '/admin/login' || $route.path === '/admin/register' || $route.path === '/admin/forget' ?false:true">
                    <div class="left_col scroll-view">
                        <div class="navbar nav_title" style="border: 0;">
                            <a href="index.html" class="site_title"><i class="fa fa-paw"></i>
                                <span>{{ !empty($data->project_name) ? $data->project_name : '' }}</span></a>
                        </div>

                        <div class="clearfix"></div>

                        <!-- menu profile quick info -->
                        <div class="profile clearfix">
                            <div class="profile_pic">
                                <img src="{{ asset('backend/employee/1655669887.jpeg') }}" alt="..."
                                    class="img-circle profile_img">
                            </div>
                            <div class="profile_info">
                                <span>Welcome,</span>
                                {{-- <h2>{{ !empty($data->name) ? ucfirst($data->name) : '' }}</h2> --}}
                                <h2 class="userValue"></h2>
                            </div>
                        </div>
                        <!-- /menu profile quick info -->

                        <br />

                        <!-- sidebar menu -->
                        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                            <div class="menu_section">
                                <h3>General</h3>
                                <ul class="nav side-menu">


                                    <li><a><i class="fa fa-list-alt"></i> Category <span
                                                class="fa fa-chevron-down"></span></a>
                                        <ul class="nav child_menu">
                                            <li>
                                                <router-link to="/admin/add-category">Add New Category
                                                </router-link>
                                            </li>

                                            <li>
                                                <router-link to="/admin/category-list">Category List</router-link>
                                            </li>
                                        </ul>
                                    </li>

                                    <li><a><i class="fa fa-pagelines"></i> Product <span
                                                class="fa fa-chevron-down"></span></a>
                                        <ul class="nav child_menu">
                                            <li>
                                                <router-link to="/admin/add-product">Add New Product</router-link>
                                            </li>
                                            <li>
                                                <router-link to="/admin/product-list">Product List</router-link>
                                            </li>
                                        </ul>
                                    </li>
                                    <li><a><i class="fa fa-list-alt"></i> Product Variation <span
                                                class="fa fa-chevron-down"></span></a>
                                        <ul class="nav child_menu">
                                            <li><a><i class="fa fa-list-alt"></i> Category
                                                    variation <span class="fa fa-chevron-down"></span></a>
                                                <ul class="nav child_menu">

                                                    <li>
                                                        <router-link to="/admin/category/wise/variation">Category
                                                            variation
                                                        </router-link>
                                                    </li>
                                                    <li>
                                                        <router-link to="/admin/category/wise/variation/list">
                                                            Category variation List
                                                        </router-link>
                                                    </li>

                                                </ul>
                                            </li>

                                            <li><a><i class="fa fa-list-alt"></i> Variation <span
                                                        class="fa fa-chevron-down"></span></a>
                                                <ul class="nav child_menu">

                                                    <li>
                                                        <router-link to="/admin/variation">variation
                                                        </router-link>
                                                    </li>
                                                    <li>
                                                        <router-link to="/admin/variation-list">variation List
                                                        </router-link>
                                                    </li>

                                                </ul>
                                            </li>
                                        </ul>
                                    </li>


                                </ul>
                            </div>
                        </div>
                        <!-- /sidebar menu -->
                    </div>
                </div>

                <!-- top navigation -->
                <div class="top_nav" id="top_nav">
                    <div class="nav_menu" style="display:none;"
                        v-show="$route.path === '/admin/login' || $route.path === '/admin/register' || $route.path === '/admin/forget'?false:true">
                        <div class="nav toggle">
                            <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                        </div>
                        <nav class="nav navbar-nav">
                            <ul class=" navbar-right">
                                <li class="nav-item dropdown open" style="padding-left: 15px;">
                                    <a href="javascript:;" class="user-profile dropdown-toggle" aria-haspopup="true"
                                        id="navbarDropdown" data-toggle="dropdown" aria-expanded="false">
                                        <img src="{{ asset('backend/employee/1655669887.jpeg') }}" alt=""><span
                                            class="userValue"></span>
                                    </a>
                                    <div class="dropdown-menu dropdown-usermenu pull-right"
                                        aria-labelledby="navbarDropdown">
                                        <a class="dropdown-item" href="javascript:;"> Profile</a>

                                        <router-link class="dropdown-item" to="logout"><i
                                                class="fa fa-sign-out pull-right"></i> Log Out</router-link>
                                    </div>
                                </li>

                                <li role="presentation" class="nav-item dropdown open">
                                    <a href="javascript:;" class="dropdown-toggle info-number" id="navbarDropdown1"
                                        data-toggle="dropdown" aria-expanded="false">
                                        <i class="fa fa-envelope-o"></i>
                                        <span class="badge bg-green">6</span>
                                    </a>
                                    <ul class="dropdown-menu list-unstyled msg_list" role="menu"
                                        aria-labelledby="navbarDropdown1">
                                        <li class="nav-item">
                                            <a class="dropdown-item">
                                                <span class="image"><img src="images/img.jpg"
                                                        alt="Profile Image" /></span>
                                                <span>
                                                    <span>John Smith</span>
                                                    <span class="time">3 mins ago</span>
                                                </span>
                                                <span class="message">
                                                    Film festivals used to be do-or-die moments for movie makers. They
                                                    were where...
                                                </span>
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="dropdown-item">
                                                <span class="image"><img src="images/img.jpg"
                                                        alt="Profile Image" /></span>
                                                <span>
                                                    <span>John Smith</span>
                                                    <span class="time">3 mins ago</span>
                                                </span>
                                                <span class="message">
                                                    Film festivals used to be do-or-die moments for movie makers. They
                                                    were where...
                                                </span>
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="dropdown-item">
                                                <span class="image"><img src="images/img.jpg"
                                                        alt="Profile Image" /></span>
                                                <span>
                                                    <span>John Smith</span>
                                                    <span class="time">3 mins ago</span>
                                                </span>
                                                <span class="message">
                                                    Film festivals used to be do-or-die moments for movie makers. They
                                                    were where...
                                                </span>
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="dropdown-item">
                                                <span class="image"><img src="images/img.jpg"
                                                        alt="Profile Image" /></span>
                                                <span>
                                                    <span>John Smith</span>
                                                    <span class="time">3 mins ago</span>
                                                </span>
                                                <span class="message">
                                                    Film festivals used to be do-or-die moments for movie makers. They
                                                    were where...
                                                </span>
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <div class="text-center">
                                                <a class="dropdown-item">
                                                    <strong>See All Alerts</strong>
                                                    <i class="fa fa-angle-right"></i>
                                                </a>
                                            </div>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
                <!-- /top navigation -->

                <!-- page content -->
                <div class="right_col" role="main">
                    <router-view></router-view>
                </div>
                <!-- /page content -->

                <!-- footer content -->
                <footer>
                    <div class="pull-right">
                        Admin Panel - Admin Template by <a href="nazmulblog.xyz">Nazmul</a>
                    </div>
                    <div class="clearfix"></div>
                </footer>
                <!-- /footer content -->
            </div>
        </div>
    </div>

    <!-- jQuery -->

    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('backend/vendors/jquery/dist/jquery.min.js') }}"></script>
    <!-- Bootstrap -->

    <script src="{{ asset('backend/vendors/bootstrap/dist/js/bootstrap.bundle.min.js') }}"></script>

    <!-- FastClick -->
    <script src="{{ asset('backend/vendors/fastclick/lib/fastclick.js') }}"></script>
    <!-- NProgress -->
    <script src="{{ asset('backend/vendors/nprogress/nprogress.js') }}"></script>
    <!-- bootstrap-wysiwyg -->
    <script src="{{ asset('backend/vendors/bootstrap-wysiwyg/js/bootstrap-wysiwyg.min.js') }}"></script>
    <script src="{{ asset('backend/vendors/jquery.hotkeys/jquery.hotkeys.js') }}"></script>
    <script src="{{ asset('backend/vendors/google-code-prettify/src/prettify.js') }}"></script>
    <!-- Custom Theme Scripts -->
    <script src="{{ asset('backend/vendors/Chart.js/dist/Chart.min.js') }}"></script>
    <!-- jQuery Sparklines -->
    <script src="{{ asset('backend/vendors/jquery-sparkline/dist/jquery.sparkline.min.js') }}"></script>
    <!-- morris.js -->
    <script src="{{ asset('backend/vendors/raphael/raphael.min.js') }}"></script>
    <script src="{{ asset('backend/vendors/morris.js/morris.min.js') }}"></script>
    <!-- gauge.js -->
    <script src="{{ asset('backend/vendors/gauge.js/dist/gauge.min.js') }}"></script>
    <!-- bootstrap-progressbar -->
    <script src="{{ asset('backend/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js') }}"></script>
    <!-- Skycons -->
    <script src="{{ asset('backend/vendors/skycons/skycons.js') }}"></script>
    <!-- Flot -->
    <script src="{{ asset('backend/vendors/Flot/jquery.flot.js') }}"></script>
    <script src="{{ asset('backend/vendors/Flot/jquery.flot.pie.js') }}"></script>
    <script src="{{ asset('backend/vendors/Flot/jquery.flot.time.js') }}"></script>
    <script src="{{ asset('backend/vendors/Flot/jquery.flot.stack.js') }}"></script>
    <script src="{{ asset('backend/vendors/Flot/jquery.flot.resize.js') }}"></script>
    <!-- Flot plugins -->
    <script src="{{ asset('backend/vendors/flot.orderbars/js/jquery.flot.orderBars.js') }}"></script>
    <script src="{{ asset('backend/vendors/flot-spline/js/jquery.flot.spline.min.js') }}"></script>
    <script src="{{ asset('backend/vendors/flot.curvedlines/curvedLines.js') }}"></script>
    <!-- DateJS -->
    <script src="{{ asset('backend/vendors/DateJS/build/date.js') }}"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="{{ asset('backend/vendors/moment/min/moment.min.js') }}"></script>
    {{-- <script src="{{ asset('backend/vendors/bootstrap-daterangepicker/daterangepicker.js') }}"></script> --}}
    {{-- editor  --}}

    <script src="https://cdn.ckeditor.com/4.16.2/standard/ckeditor.js"></script>
    <script src="{{ asset('backend/build/js/custom.min.js') }}"></script>
    <script>
        let token = localStorage.getItem('token');
        if (token) {
            $("#left_col").css('display', '');
            $("#top_nav").css('display', '');
            $("#App").css('background', '');
            $(".right_col").css('margin-left', '');
        }


        let userValue = User.name()
        $('.userValue').html(userValue)

        // all Data Show in Dashboard 
    </script>




</body>

</html>

<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Image;
use DB;

class SupplierController extends Controller
{


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $suppliers = DB::table('suppliers')->get();
        return response()->json($suppliers);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        if($request->photo != ''){
            $img_data = $request->photo;
            $positions = strpos($img_data,';');
            $sub_img = substr($img_data, 0, $positions);
            $img_path = explode('/',$sub_img)[1];
            $img_name = time() . '.' . $img_path;
            $img = Image::make($img_data)->resize(240,200);
            $upload_path = 'backend/supplier/';
            $image_url = $upload_path .$img_name;
            $img->save($image_url);

            $data = array();
            $data['name'] = $request->full_name;
            $data['email'] = $request->email_address;
            $data['address'] = $request->address;
            $data['shop_name'] = $request->shop_name;
            $data['phone'] = $request->phone;
            $data['photo'] = $image_url;
            DB::table("suppliers")->insert($data);
        }else{
            $data = array();
            $data['name'] = $request->full_name;
            $data['email'] = $request->email_address;
            $data['address'] = $request->address;
            $data['shop_name'] = $request->shop_name;
            $data['phone'] = $request->phone;
            DB::table("suppliers")->insert($data);
        }


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = DB::table('suppliers')->where('id', $id)->first();
        return response()->json($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request, $id)
    {
        $data = array();
        $data['name'] = $request->name;
        $data['email'] = $request->email;
        $data['address'] = $request->address;
        $data['shop_name'] = $request->shop_name;
        $data['phone'] = $request->phone;
        $update_img = $request->newPhoto;

        if($update_img != ''){
            $positions = strpos($update_img,';');
            $sub_img = substr($update_img, 0, $positions);
            $ext = explode('/',$sub_img)[1];
            $img = Image::make($update_img)->resize(240,200);
            $img_name = time() . '.' . $ext;
            $upload_path = 'backend/supplier/';
            $img_path = $upload_path . $img_name;
            $success = $img->save($img_path);

            if($success){
                $data['photo'] = $img_path;
                $data_dt = DB::table('suppliers')->where('id', $id)->first();
                $img_path_dt = $data_dt->photo;
                $done = unlink($img_path_dt);
                $user = DB::table('suppliers')->where('id', $id)->update($data);
            }
        }else{
               $mg_photo = $request->photo;
               $data['photo'] = $mg_photo;
               $user = DB::table('suppliers')->where('id', $id)->update($data);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $data = DB::table('suppliers')->where('id',$id)->first();
       if ($data->photo != ''){
        DB::table('suppliers')->where('id',$id)->delete();
        unlink($data->photo);
       }else{
        DB::table('suppliers')->where('id',$id)->delete();
       }
    }
}

<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Product;
use Illuminate\Http\Request;
use DB;
use Image;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = DB::select("SELECT
        item.*,
        cat.`category_name`,
        var.`name` AS var_name,
        opt.`value` v_opt_value
      FROM
        product item
        LEFT JOIN product_categories cat
          ON item.`category_no_fk` = cat.`id`
        LEFT JOIN variation var
          ON var.`category_id` = cat.`id`
        LEFT JOIN variation_option opt
          ON var.`id` = opt.`variation_id`");
        return response()->json($data);
    }

    public function productWiseCategoryList(){
        $cat_id = request('cat_id');
        $catwvariation = DB::table('variation')->where('category_id',$cat_id)->get();
        return response()->json($catwvariation);
    }
    public function catWiseVariationList(){
        $cat_id = request('cat_id');
        $catwvariation = DB::table('variation')->where('category_id',$cat_id)->get();
        return response()->json($catwvariation);
    }
    
    public function variationWiseVariationList(){
        $var_id = request('var_id');        
        $data = DB::table('variation_option')->where('variation_id',$var_id)->get();
        return response()->json($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
        dd($request->all());
        // $validated = $request->validate([
        //     'full_name' => 'required',
        //     'email_address' => 'required',
        //     'address' => 'required',
        // ]);

        if ($request->photo != '') {
            $img_data = $request->photo;
            $positions = strpos($img_data, ';');
            $sub_img = substr($img_data, 0, $positions);
            $img_path = explode('/', $sub_img)[1];
            $img_name = time() . '.' . $img_path;
            $img = Image::make($img_data)->resize(240, 200);
            $upload_path = 'backend/employee/';
            $image_url = $upload_path . $img_name;
            $img->save($image_url);

            $product = new Product();
            $product->category_no_fk = $request->category_id;
            $product->name = $request->Product_name;
            $product->description = $request->buying_price;
            $product->variation_no_fk = $request->variation_option_id;
            $product->variationoption_no_fk = $request->variation_option_id;
            $product->status = $request->quantity;
            $product->product_image = $image_url;
            $product->save();
        } else {
            $product = new Product();
            $product->product_code = $request->product_code;
            $product->product_name = $request->Product_name;
            // $product->category_id = $request->category_id;
            $product->variationoption_no_fk = $request->variation_option_id;
            // $product->buying_price = $request->buying_price;
            $product->sales_price = $request->sell_price;
            // $product->supplier_id = $request->supplier_id;
            $product->quantity = $request->quantity;
            $product->save();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = DB::table('products')->where('id', $id)->first();
        if ($data->photo == '') {
            unlink($data->photo);
            DB::table('productss')->where('id', $id)->delete();
        } else {

            DB::table('productss')->where('id', $id)->delete();
        }
    }

    //category WIse Variation 
    public function category_wise_variation(){
        $categoryVariation = DB::select(DB::raw("SELECT
        var.status,
        var.name,
        cat.`category_name`,
        var.`id`,
        cat.`parent_category_id`
      FROM
        variation var
        JOIN product_categories cat
          ON var.`category_id` = cat.`id`"));
        return response()->json($categoryVariation);
    }
    public function storeCategoryWiseVariation(Request $request){
        // $validated = $request->validate([
        //     'category_id' => 'required',
        //     'variation_name' => 'required'
        // ]);

        $data = array();
        $data['category_id'] = $request->category_id;
        $data['name'] = $request->value;
        $data['status'] = $request->cat_status;
        DB::table('variation')->insert($data);
    }
    public function editCategoryWiseVariation($id){
        $data = DB::table('variation')->where('id',$id)->first();      
        return response()->json($data);
    }
    
    // Variation 
    public function storeVariation(Request $request){
        // $validated = $request->validate([
        //     'category_id' => 'required',
        //     'variation_name' => 'required'
        // ]);

        $data = array();
        $data['variation_id'] = $request->variation_id;
        $data['value'] = $request->variation_option_name;
        $data['status'] = $request->cat_status;
        DB::table('variation_option')->insert($data);
    }

    
public function category_variation_List(){
        $categoryVariationOption = DB::select(DB::raw("SELECT
        var.status,
        var.name,
        cat.`category_name`,
        var.`id`,
        cat.`parent_category_id`,
        opt.`value`
      FROM
        variation var
        JOIN product_categories cat
          ON var.`category_id` = cat.`id`
        LEFT JOIN variation_option opt
          ON cat.`id` = opt.`variation_id`"));
        return response()->json($categoryVariationOption);
    }

    
}

<?php

namespace App\Http\Controllers\Api;

use App\Models\Salary;
use App\Http\Controllers\Controller;
use App\Http\Requests\SalaryRequest;

class SalaryController extends Controller
{
    public function paySalary(SalaryRequest $request){
        Salary::create([
            'employee_id'=>$request->id,
            'amount'=>$request->salary,
            'salary_year'=>$request->year,
            'salary_month'=>$request->month,
            'salary_date'=> date('Y-m-d')
        ]);
    }
}

<?php

namespace App\Http\Controllers\Api;

use DB;
use App\Models\Employee;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Image;
class EmployeeController extends Controller
{

    public function index(){
        $employee = DB::table('Employees')->get();
        return response()->json($employee);
    }

    public function store(Request $request){
        // $validated = $request->validate([
        //     'full_name' => 'required',
        //     'email_address' => 'required',
        //     'address' => 'required',
        // ]);

        if($request->photo != ''){
            $img_data = $request->photo;
            $positions = strpos($img_data,';');
            $sub_img = substr($img_data, 0, $positions);
            $img_path = explode('/',$sub_img)[1];
            $img_name = time() . '.' . $img_path;
            $img = Image::make($img_data)->resize(240,200);
            $upload_path = 'backend/employee/';
            $image_url = $upload_path .$img_name;
            $img->save($image_url);

            $employee = new Employee();
            $employee->name = $request->full_name;
            $employee->email = $request->email_address;
            $employee->address = $request->address;
            $employee->phone = $request->phone;
            $employee->salary = $request->salary;
            $employee->joining_date = $request->date;
            $employee->nid = $request->nid;
            $employee->photo = $image_url;
            $employee->save();
        }else{
            $employee = new Employee();
            $employee->name = $request->full_name;
            $employee->email = $request->email_address;
            $employee->address = $request->address;
            $employee->phone = $request->phone;
            $employee->salary = $request->salary;
            $employee->joining_date = $request->date;
            $employee->nid = $request->nid;
            $employee->save();
        }
    }
public function show($id){
    $data = DB::table('employees')->where('id',$id)->first();
    // dd($data);
    return response()->json($data);
}
    public function destroy($id){
        $emp_dt = DB::table('employees')->where('id',$id)->first();
        if($emp_dt->photo == ''){
            unlink($emp_dt->photo);
            DB::table('employees')->where('id', $id)->delete();
        }else{

            DB::table('employees')->where('id', $id)->delete();
        }
    }

    public function update(Request $request, $id){

        $data = array();
        $data['name'] = $request->name;
        $data['email'] = $request->email;
        $data['phone'] = $request->phone;
        $data['address'] = $request->address;
        $data['joining_date'] = $request->joining_date;
        $data['nid'] = $request->nid;
        $data['salary'] = $request->salary;
        $update_photo = $request->newPhoto;

        if($update_photo){
            $img_data = $update_photo;
            $positions = strpos($img_data,';');
            $sub_img = substr($img_data, 0, $positions);
            $img_path = explode('/',$sub_img)[1];
            // dd($img_path);
            $img_name = time() . '.' . $img_path;
            $img = Image::make($img_data)->resize(240,200);
            $upload_path = 'backend/employee/';
            $image_url = $upload_path.$img_name;
            $success = $img->save($image_url);

            if($success){
                $data['photo'] = $image_url;
                $img_data_list = DB::table('employees')->where('id', $id)->first();
                // dd($img_data);
                $img_path_dt = $img_data_list->photo;
                $done = unlink($img_path_dt);
                $user = DB::table('employees')->where('id', $id)->update($data);
            }
            return response()->json('image');

        }else{
            $oldLogo = $request->photo;
            $data['photo'] = $oldLogo;
            $user = DB::table('employees')->where('id', $id)->update($data);
        }
    }
}

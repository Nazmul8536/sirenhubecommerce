<?php

namespace App\Http\Controllers;

use DB;
use Auth;

use Illuminate\Http\Request;

class FrontEndController extends Controller
{
  public function index()
  {
    $categories = DB::table('product_categories')->where('status', 1)->get();
    return view('index', [
      'categories'  => $categories,
      'auth_user' => Auth::user()
    ]);
  }


  //whole sale 
  public function wholeSale()
  {

    $categories = DB::table('product_categories')->where('status', 1)->get();

    return view('wholesale.wholesale', [
      'categories'  => $categories
    ]);
  }

  public function productDetails($id)
  {
    # code...
    $data = DB::select("SELECT
        item.*,
        cat.`category_name`,
        var.`name` AS var_name,
        opt.`value` v_opt_value
      FROM
        product item
        LEFT JOIN product_categories cat
          ON item.`category_no_fk` = cat.`id`
        LEFT JOIN variation var
          ON var.`category_id` = cat.`id`
        LEFT JOIN variation_option opt
          ON var.`id` = opt.`variation_id` where item.id = $id");
    //    dd($data);
    return response()->json($data);
  }

  //cart 
  public function storeCartItem(Request $request)
  {
    // dd($request->all());
    date_default_timezone_set('Asia/Dhaka');

    $cat_id = DB::table('product')->select('category_no_fk')->where('id', $request->product_id)->first();
    $product = DB::table('product')->select('category_no_fk')->where('id', $request->product_id)->first();
    // dd($cat_id->category_no_fk);


    // $data = array();
    // $data['product_item_id'] = $request->product_id; //$request->product_id

    // $data['cart_id'] = $cat_id->category_no_fk;
    // $data['qty'] = 1;
    // $data['status'] = "SELECT";
    // $data['created_at'] = date("Y-m-d H:i:s");
    // $data['updated_at'] = date("Y-m-d H:i:s");
    // DB::table('shopping_cart_item')->insert($data);

    // $cart_dt = array();
    // $cart_dt['user_id'] = $request->user_id;
    // $cart_dt['status'] = "SELECT";
    // $cart_dt['created_at'] = date("Y-m-d H:i:s");
    // $cart_dt['updated_at'] = date("Y-m-d H:i:s");
    // DB::table('shopping_cart')->insert($cart_dt);

    return response()->json($product);
  }
}

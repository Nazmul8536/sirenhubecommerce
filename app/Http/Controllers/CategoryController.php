<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Models\Category;
use Image;

class CategoryController extends Controller
{
    public function categories(){
        $data = DB::table('product_categories')->where('status',1)->get();
        return response()->json($data);
    }


    public function storeCategory(Request $request){
        $validated = $request->validate([
            'category_name' => 'required',
        ]);
        $id = DB::select('SELECT id FROM product_categories ORDER BY ID DESC LIMIT 1');
        //  dd($id);
        if($id == null){
            $id = 1;
        }else{
            $id = $id[0]->id + 1;
        }
        $data = array();
        $data['category_name'] = $request->category_name;
        $data['parent_category_id'] = 'cat-'.$id;
        $data['category_img'] = $request->category_icon;
        $data['status'] = $request->cat_status;
        $data['id'] = $id;
        // if ($request->photo != '') {
            // $img_data = $request->photo;
            // $positions = strpos($img_data, ';');
            // $sub_img = substr($img_data, 0, $positions);
            // $img_path = explode('/', $sub_img)[1];
            // $img_name = time() . '.' . $img_path;
            // $img = Image::make($img_data)->resize(240, 200);
            // $upload_path = 'backend/categories/';
            // $image_url = $upload_path . $img_name;
            // $img->save($image_url);
            // $data['category_img'] = $image_url;
            DB::table('product_categories')->insert($data);
            
        // } else {
        //     DB::table('product_categories')->insert($data);
        // }
    }
    public function deleteCategory($id){
        DB::table('product_categories')->where('id', $id)->delete();
    }

    public function editCategory($id){
        $data = DB::table('product_categories')->where('id',$id)->first();
        return response()->json($data);
    }

    public function updateCategory(Request $request,$id){
        $validated = $request->validate([
            'category_name' => 'required',
        ]);

        // $category = Category::findOrfail($id);
        // $category['category_name'] = $request->category_name;
        // $category->save();
        $data = array();
        $data['category_name'] = $request->category_name;
        $data['category_img'] = $request->category_icon;
        $data['status'] = $request->cat_status;
        DB::table('product_categories')->where('id',$id)->update($data);
    }
}

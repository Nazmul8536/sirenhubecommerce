<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;
    protected $fillable = [
        'category_no_fk',
        'name',
        'description',
        'variation_no_fk',
        'variationoption_no_fk',
        'status',
        'product_image', ];
}
